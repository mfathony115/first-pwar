import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import CssBaseLine from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Container from  '@material-ui/core/Container';
import LockOutlineIcons from '@material-ui/icons/LockOutlined';
import FormSignin from '../../component/form-login';
import Title from '../../component/title';
import Copyright from '../../component/copyright';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar:{
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    }
}))

export default function Component(){
    const classes = useStyles();
    return(
        <Container component="main" maxWidth="xs">
            <CssBaseLine />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlineIcons />
                    </Avatar>
                    <Title name="Sign In" />
                    <FormSignin />
                </div>
                <Box mt={8}>
                    <Copyright />     
                </Box>
        </Container>
    )
}