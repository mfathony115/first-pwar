import React from 'react'
import { Avatar, Container, CssBaseline, Box } from '@material-ui/core'
import { LockOpenOutlined } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import Title from '../../component/title';
import Copyright from '../../component/copyright';
import FormSignup from '../../component/form-register';
const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar:{
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    }
}))

export default function Component(){
    const classes = useStyles();
    return(
        <Container component="main" maxWidth="xs">
            <CssBaseline>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOpenOutlined />
                    </Avatar>
                    <Title name="Sign Up" />
                    <FormSignup />
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </CssBaseline>
        </Container>
    )
}