const BaseUrl = process.env.REACT_APP_BASE_URL;
export const PostApi = (url, data) => {
    return fetch(`${BaseUrl}/${url}`, {
        method: 'POST',
        body: data        
    });
};

export const statusApi = response => {
    if(response.status !== 200){
        return Promise.reject(new Error(response.statusText));
    } else {
        return Promise.resolve(response);
    }
};

export const jsonApi = response => {
    return response.json();
};

export const errorApi = error => console.log(`Error: ${error}`);