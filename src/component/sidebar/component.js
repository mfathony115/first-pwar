import React from 'react';
import { NavLink } from 'react-router-dom'
//material UI
import List from "@material-ui/core/List";
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
//Daftar Sidebar

const Component = (props) => {

    const {routes} = props;
    const links = (
        <List>
            {
                routes.map((prop, key)=> {
                    return(
                        <NavLink 
                         to={prop.path}
                         key={key}>
                            <ListItem button>
                                <ListItemIcon>
                                    <DashboardIcon />
                                </ListItemIcon>
                                <ListItemText primary={prop.sidebarName} />
                            </ListItem>
                        </NavLink>
                    );
                })
            }
        </List>
    );
    return(
        <div>
            {links}
        </div>
    )
}

export default Component;