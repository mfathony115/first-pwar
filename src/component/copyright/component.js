import React from 'react'
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
const Component = () => {
    return(
        <div>
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href="#">
                    SIMRS Tugurejo
                </Link>
                {' '}
                { new Date().getFullYear()}
                {'.'}
            </Typography>
        </div>
    )
}
export default Component;