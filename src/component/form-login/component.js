import React, {useState} from 'react'
import { 
    Button, 
    TextField, 
    Grid, 
    Box,
    FormControlLabel
} from '@material-ui/core'
import CheckBox from '@material-ui/core/Checkbox';
import { makeStyles }  from '@material-ui/core/styles';
import { Link, withRouter } from 'react-router-dom';
import { Login } from '../../services/login';
import { PostApi, statusApi, jsonApi, errorApi } from '../../config/api';

const useStyles = makeStyles ((theme) => ({
    form: {
        width: '100%',
        marginTop: theme.spacing(1)
    },
    submit:{
        margin: theme.spacing(3, 0, 2)
    }
}));
const Component = (props) => {
    const formData = new FormData();
    const classes = useStyles();
    const [dataLogin, setDataLogin] = useState({
        username: '',
        password: ''
    });
    const handleChange = (event) => {
        event.persist();
        setDataLogin((currenValues) => ({
            ...currenValues,
            [event.target.name] : event.target.value,
        }));
    };
    const handleSubmit = event => {
        event.preventDefault();
        formData.append('username', dataLogin.username);
        formData.append('password', dataLogin.password);
        // PostApi('login', formData)
        // .then(statusApi)
        // .then(jsonApi)
        // .then(data => {
        //     props.history.push('/dashboard');
        // })
        // .catch(errorApi);
        Login(formData)
            .then((res) => {
                props.history.push('/dashboard');
            })
            .catch((error) => {
                alert(error.message);
            })
    }
    return(
        <React.Fragment>
            <Box>
            <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField 
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        autoComplete="username"
                        autoFocus
                        value={dataLogin.username}
                        onChange={handleChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={dataLogin.password}
                        onChange={handleChange}
                    />
                    <FormControlLabel  
                        control={<CheckBox value="remember" color="primary" />}
                        label="Remember Me"
                    />
                    <Button 
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>
                    <Grid container>
                        <Grid item xs>
                            <Link to="/forgot-password" >
                                Forgot Password
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link to="/signup" >
                                Don't have an account ? Sign Up
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </Box>
        </React.Fragment>
    )
}

export default withRouter(Component);