import React from 'react'
import Typography from '@material-ui/core/Typography';

const Component = props => {
    return(
        <div>
            <Typography component="h1" variant="h5" >
                {props.name}
            </Typography>
        </div>
    )
}

export default Component;