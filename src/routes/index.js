import Dashboard from '../pages/dashboard';
import Orders from '../pages/orders';
// material UI icon
import DashboardIcon from '@material-ui/icons/Dashboard';
import MenuIcon from '@material-ui/icons/Menu';

const pwaRoutes = [
    {
        path: "/dashboard",
        sidebarName: "Dashboard",
        navbarName: "Dashboard",
        component: Dashboard,
        icon: DashboardIcon,
     
    }, {
        path: "/orders",
        sidebarName: "Orders",
        navbarName: "Orders",
        component: Orders,
        icon: MenuIcon,
      
    }
];

export default pwaRoutes;

