import { axiosInstance }  from '../config' ;

export const Login = async data => {
    const response = axiosInstance.post('login', data);
    return response;
};