import React from 'react'
import { Switch, Route, BrowserRouter} from 'react-router-dom'; 
import Signin from './pages/signin';
import Signup from './pages/signup';
import Dashboard from './layouts';
import Orders from './pages/orders';
import './App.css';
function App() {
  return (
   <React.Fragment>
     <BrowserRouter>
        <Switch>
          <Route path="/" exact>
            <Signin />
          </Route>
          <Route path="/signin" exact >
            <Signin />
          </Route>
          <Route path="/signup" exact>
            <Signup />
          </Route>
          <Route path="/dashboard" exact>
            <Dashboard />
          </Route>
          <Route>
            <Orders />
          </Route>
        </Switch>
     </BrowserRouter>
   </React.Fragment>
  );
}

export default App;
